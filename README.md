# Karte kartice #

Simple ticketing test app that uses Last.fm API to fetch music events near the location that user specifies. After logging in, user can search for events, see band description, get location of the venue and, of course, buy tickets! But only for virtual currency :)

App is for Android platform, back-end is written in PHP/MySQL and it provides custom API for communicating with the app and Last.fm API for fetching events, bands and venues.