package org.nodinodi.kartekartice.app;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

public class EventsArrayAdapter extends ArrayAdapter<EventItem> {


    WindowManager wm;
    SessionManager manager;
    private List<EventItem> items;
    Context currentContext;

    public EventsArrayAdapter(Context context, int resource, List<EventItem> items, WindowManager _wm) {

        super(context, resource, items);

        this.items = items;
        this.wm = _wm;
        this.manager = new SessionManager(context);
        this.currentContext = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if (v == null) {

            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.events_list_item, null);

        }

        EventItem p = items.get(position);

        if (p != null) {

            TextView name = (TextView) v.findViewById(R.id.eventName);
            TextView locationId = (TextView) v.findViewById(R.id.eventLocationId);
            TextView datePerformance = (TextView) v.findViewById(R.id.eventDatePerformance);

            if (name != null) {
                name.setText(" " + p.getName());
                Log.d("TAG", p.getName());
            }
            if (locationId != null) {
                locationId.setText(" " + p.getLocationId());
                Log.d("TAG", p.getLocationId());
            }
            if (datePerformance != null) {
                datePerformance.setText(" " + p.getDatePerformance());
            }
        }

        return v;

    }
}