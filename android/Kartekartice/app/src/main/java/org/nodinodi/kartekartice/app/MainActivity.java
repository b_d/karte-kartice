package org.nodinodi.kartekartice.app;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends ActionBarActivity {

    EditText etUsername;
    EditText etPassword;
    ImageButton ibLogin;
    SessionManager session;
    final String login_link = "http://yourdomain.net/login.php";
    // Progress Dialog
    private ProgressDialog pDialog;
    // JSON parser
    JSONParser jParser = new JSONParser();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        session = new SessionManager(getApplicationContext());

        if(!isNetworkConnected()){
            new AlertDialog.Builder(this)
                    .setTitle("Mreža!")
                    .setMessage("Čovjek, provjeri si internetsku vezu!!")
                    .setPositiveButton("Close", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // continue
                            finish();
                        }
                    })
                    .show();
        }
        init();
    }


    /**
     * This method will set and define activity ui items
     */
    private void init() {
        etUsername = (EditText) findViewById(R.id.etUsername);
        etPassword = (EditText) findViewById(R.id.etPassword);
        ibLogin = (ImageButton) findViewById(R.id.imageButton1);

        ibLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    if(etUsername.getText().toString().equals("") || etPassword.getText().toString().equals("")){
                        Toast.makeText(getApplicationContext(),"Nećemo tako, unesi kredencije!!", Toast.LENGTH_SHORT).show();
                    }else{
                        Login login = new Login();
                        login.execute();
                    }
                } catch (Exception e) {
                    Log.e("Dino", "Nemože login");
                }
            }
        });

    }
    public void asd(View v) {
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.youtube.com/watch?v=x0-EyHT1lJk")));

    }
    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        if (ni == null) {
            // There are no active networks.
            return false;
        } else
            return true;
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    /**
     * Background Async Task to login to app
     */
    class Login extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Dialog
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(MainActivity.this);
            pDialog.setMessage("Čekaj da te provjerimo");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        /**
         * Creating http request for login
         */
        protected String doInBackground(String... args) {
            final String username = etUsername.getText().toString();
            final String password = etPassword.getText().toString();

            JSONObject json = null;
            // Building Parameters
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("username", username));
            params.add(new BasicNameValuePair("password", password));

            // getting JSON Object
            // Note that create product url accepts POST method
            json = jParser.makeHttpRequest(login_link, "POST", params);

            // check log cat for response
            Log.d("JSON", json.toString());

            // check for success tag
            try {

                Integer status = json.getInt("status");
                if(status == 1){
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            //manager.createLoginSession(username);

                            session.createLoginSession(username);
                            Toast.makeText(getApplicationContext(), username + " ulogiran si!", Toast.LENGTH_SHORT).show();
                            Intent i = new Intent();
                            i.setClass(MainActivity.this, LoggedIn.class);
                            startActivity(i);
                        }
                    });
                } else {
                    Log.d("Dino", "Ne prolazi username i pass dio");
                    runOnUiThread(new Runnable() {

                        @Override
                        public void run() {

                            Toast.makeText(getApplicationContext(), "Ajoj, krivi username ili password!", Toast.LENGTH_LONG).show();
                        }
                    });

                }
            } catch (JSONException e) {
                Log.e("JSON exception", e.toString());
                e.printStackTrace();
            } catch (Exception e) {
                Log.e("Exception", e.toString());
                e.printStackTrace();
            }

            return null;
        }

        protected void onPostExecute(String file_url) {
            // dismiss the dialog once done
            pDialog.dismiss();
        }

    }
    public void goRegister(View v) {
        Intent i = new Intent(getApplicationContext(), Register.class);
        startActivity(i);
    }
}
