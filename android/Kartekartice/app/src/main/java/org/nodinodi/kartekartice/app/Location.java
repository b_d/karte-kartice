package org.nodinodi.kartekartice.app;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.NetworkOnMainThreadException;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;


public class Location extends ActionBarActivity {

    TextView txtName, txtAddress, txtDate, txtDesc;
    ImageView picture;
    private ProgressDialog pDialog;;
    JSONParser jParser = new JSONParser();
    final String link = "http://yourdomain.net/getLocationInfo.php";
    String locationId, name, street, post_num, city, date, desc;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location);
        Intent intent;
        intent = getIntent();
        String id = intent.getStringExtra("id");



        setupData();
    }

    private void setupData() {
        Intent intent;
        intent = getIntent();
        locationId = intent.getStringExtra("id");
        txtName = (TextView)findViewById(R.id.txtName);
        txtAddress = (TextView)findViewById(R.id.txtAddress);
        txtDesc = (TextView)findViewById(R.id.txtDesc);
        picture = (ImageView)findViewById(R.id.ivMap);

        ScrollView scrollview2 = (ScrollView)findViewById(R.id.scrollView1);

        scrollview2.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                txtDesc.getParent().requestDisallowInterceptTouchEvent(false);
                return false;
            }
        });


        txtDesc.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                txtDesc.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });

        new getLocationInfo().execute();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.location, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    public void change_values(){
        txtName.setText(name);
        txtAddress.setText("Adresa: " +  street + ", " +post_num  + " " + city);
        txtDesc.setText(desc);
        setTitle(name);
    }

    class getLocationInfo extends AsyncTask<String, String, String> {


        String image_link;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(Location.this);
            pDialog.setMessage("Samo malo...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        protected String doInBackground(String... args) {

            JSONObject json = null;
            // Building Parameters
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("id", locationId));

            json = jParser.makeHttpRequest(link, "GET", params);

            // check log cat for response
            Log.d("JSON", json.toString());

            // check for success tag

            try {
                name = json.getString("naziv");
                street = json.getString("ulica");
                post_num = json.getString("postanski_broj");
                city = json.getString("naselje");
                desc = json.getString("opis_lokacije");

                image_link = json.getString("image_link");
                /*
                trailer = json.getString("trailer");

                if(trailer.equals("")){
                    disable_trailer_button();
                }
                id_location = json.getString("id_lokacija");
*/
            } catch (JSONException e) {
                Log.e("JSON exception", e.toString());
                e.printStackTrace();
            } catch (Exception e) {
                Log.e("Exception", e.toString());
                e.printStackTrace();
            }

            return null;
        }

        protected void onPostExecute(String file_url) {
            // dismiss the dialog once done
            Log.d("Das", "dd");
            Log.d("Das", image_link);
            change_values();

            //new DownloadImageTask((ImageView) findViewById(R.id.ivMap)).execute(image_link);
            new RetrieveImage(picture).execute(image_link);
            pDialog.dismiss();
        }

    }

    class RetrieveImage extends AsyncTask<String, Void, Bitmap> {

        ImageView map;

        public RetrieveImage(ImageView _map) {
            this.map = _map;
        }

        protected Bitmap doInBackground(String... urls) {

            Bitmap bitmap = null;
            InputStream is = null;
            try {
                DefaultHttpClient httpClient = new DefaultHttpClient();
                httpClient.getParams().setParameter("http.protocol.content-charset", "UTF-8");
                HttpGet httpGet = new HttpGet(urls[0].replace(" ", "%20"));
                HttpResponse httpResponse = httpClient.execute(httpGet);
                HttpEntity httpEntity = httpResponse.getEntity();
                is = httpEntity.getContent();
                bitmap = BitmapFactory.decodeStream(is);

            } catch (IOException e) {
                e.printStackTrace();
            } catch (NetworkOnMainThreadException e) {
                Log.e("NETWORK ERROR", "Image" + e.toString());
            } catch (NullPointerException e) {
                Log.e("NETWORK ERROR", "Image" + e.toString());
            } catch (RuntimeException e) {
                Log.e("NETWORK ERROR", "Image" + e.toString());
            }

            return bitmap;
        }

        protected void onPostExecute(Bitmap _bitmap) {
            // TODO: check this.exception
            // TODO: do something with the feed
            map.setImageBitmap(_bitmap);
        }
    }


}
