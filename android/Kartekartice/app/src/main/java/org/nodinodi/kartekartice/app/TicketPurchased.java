package org.nodinodi.kartekartice.app;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dino Bikić on 25.5.2014..
 */
public class TicketPurchased extends Activity{

    Button btnReturn;
    JSONParser jParser = new JSONParser();
    SessionManager manager;
    final String link = "http://yourdomain.net/setPurchasedTicket.php";
    String id_event;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ticket_purchased);

        btnReturn = (Button)findViewById(R.id.button);
        btnReturn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent();
                in.setClass(getApplicationContext(), LoggedIn.class);
                startActivity(in);
            }
        });

        manager = new SessionManager(getApplicationContext());

        Intent intent;
        intent = getIntent();
        id_event = intent.getStringExtra("id_event");

        new updateDatabase().execute();
    }

    class updateDatabase extends AsyncTask<String, String, String> {

        protected String doInBackground(String... args) {

            JSONObject json = null;
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("id_event", id_event));
            params.add(new BasicNameValuePair("username", manager.getUsername()));
            jParser.makeHttpRequest(link, "GET", params);

            return null;
        }


    }
  }

