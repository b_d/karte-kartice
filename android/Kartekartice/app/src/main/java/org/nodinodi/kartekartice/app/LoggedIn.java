package org.nodinodi.kartekartice.app;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class LoggedIn extends Activity {


    ImageButton ibKategorije, ibGradovi, ibSreca;
    String event_id;
    final String getKategorije = "http://yourdomain.net/getKategorije.php";
    final String getCities = "http://yourdomain.net/getCities.php";
    final String getLucky = "http://yourdomain.net/getLucky.php";
    ArrayList<ArrayList<String>> kategorijeLista;
    SessionManager manager;

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Toast.makeText(getApplicationContext(), manager.getUsername()+" odlogirao si se :(", Toast.LENGTH_LONG).show();
        manager.logoutUser();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logged_in);
        manager = new SessionManager(getApplicationContext());

        if(!isNetworkConnected()){
            new AlertDialog.Builder(this)
                    .setTitle("NETWORK")
                    .setMessage("Čovjek, provjeri si internetsku vezu!!")
                    .setPositiveButton("Zatvori me", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // continue
                            finish();
                        }
                    })
                    .show();
        }
        init();
    }
    private void init() {
        ibKategorije = (ImageButton) findViewById(R.id.ibKaregorije);
        ibKategorije.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new ActivitiesGetter().execute();
            }
        });
        ibGradovi = (ImageButton) findViewById(R.id.ibGradovi);
        ibGradovi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new CitiesGetter().execute();
            }
        });

        ibSreca = (ImageButton) findViewById(R.id.ibSreca);
        ibSreca.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new Luck().execute();
            }
        });
    
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        if (ni == null) {
            // There are no active networks.
            return false;
        } else
            return true;
    }

    private class ActivitiesGetter extends AsyncTask<Void, Void, Void> {

        ArrayList<ArrayList<String>> kategorije = new ArrayList<ArrayList<String>>();
        JSONObject jsonObject;

        @Override
        protected void onPostExecute(Void aVoid) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Intent i = new Intent();
                    i.setClass(getApplicationContext(), Categories.class);
                    i.putExtra("categories",jsonObject.toString());
                    startActivity(i);

                }
            });
        }

        @Override
        protected Void doInBackground(Void... voids) {
            JSONParser jsonParser = new JSONParser();
            jsonObject = jsonParser.makeHttpRequest(getKategorije, "GET", null);
            return null;
        }
    }

    private class CitiesGetter extends AsyncTask<Void, Void, Void> {

        ArrayList<ArrayList<String>> gradovi = new ArrayList<ArrayList<String>>();
        JSONObject jsonObject;

        @Override
        protected void onPostExecute(Void aVoid) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Intent i = new Intent();
                    i.setClass(getApplicationContext(), Cities.class);
                    i.putExtra("cities",jsonObject.toString());
                    startActivity(i);

                }
            });
        }

        @Override
        protected Void doInBackground(Void... voids) {
            JSONParser jsonParser = new JSONParser();
            jsonObject = jsonParser.makeHttpRequest(getCities, "GET", null);
            return null;
        }
    }
    private class Luck extends AsyncTask<Void, Void, Void> {

        ArrayList<ArrayList<String>> gradovi = new ArrayList<ArrayList<String>>();
        JSONObject jsonObject;

        @Override
        protected Void doInBackground(Void... voids) {
            JSONParser jsonParser = new JSONParser();
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("user", manager.getUsername()));
            jsonObject = jsonParser.makeHttpRequest(getLucky, "GET", params);

            try {
                Integer status = jsonObject.getInt("status");
                if(status == 1){

                    event_id = jsonObject.getString("event");
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            Intent in = new Intent();
                            in.setClass(getApplicationContext(), Event.class);
                            in.putExtra("id",event_id);
                            startActivity(in);

                        }
                    });

                } else {

                    runOnUiThread(new Runnable() {

                        @Override
                        public void run() {

                            Toast.makeText(getApplicationContext(), "Za koristenje ove opcije moras kupiti bar jednu kartu!", Toast.LENGTH_LONG).show();
                        }
                    });

                }
            } catch (JSONException e) {
                Log.e("JSON exception", e.toString());
                e.printStackTrace();
            } catch (Exception e) {
                Log.e("Exception", e.toString());
                e.printStackTrace();
            }
            return null;
        }
    }

}
