package org.nodinodi.kartekartice.app;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.NetworkOnMainThreadException;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;


public class Event extends ActionBarActivity {

    TextView txtTitle, txtVenue, txtDate, txtDesc;
    ImageView picture;
    Button btnPurchase;
    String image_name;
    String eventId, title, venue, date, desc;
    private ProgressDialog pDialog;;
    JSONParser jParser = new JSONParser();
    final String link = "http://yourdomain.net/getEventInfo.php";
    String imgName, id_location, trailer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event);

        setupData();
    }

    private void setupData() {
        Intent intent;
        intent = getIntent();
        eventId = intent.getStringExtra("id");
        setTitle("Detaljno");

        txtTitle = (TextView)findViewById(R.id.txtTitle);
        txtDate = (TextView)findViewById(R.id.txtDate);
        txtVenue = (TextView)findViewById(R.id.txtVenue);
        txtDesc = (TextView)findViewById(R.id.txtDesc);
        btnPurchase = (Button)findViewById(R.id.btnPurchase);

        btnPurchase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        Intent in = new Intent();
                        in.setClass(getApplicationContext(), TicketPurchased.class);
                        in.putExtra("id_event", eventId);
                        in.putExtra("id_user",id_location);
                        startActivity(in);
                    }
                });
            }
        });
        ScrollView scrollview1 = (ScrollView)findViewById(R.id.scrollView);

        scrollview1.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                txtDesc.getParent().requestDisallowInterceptTouchEvent(false);
                return false;
            }
        });


        txtDesc.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                txtDesc.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });

        new getEventInfo().execute();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.event, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void Location(View v) {
        runOnUiThread(new Runnable() {

            @Override
            public void run() {
                Intent in = new Intent();
                in.setClass(getApplicationContext(), Location.class);
                in.putExtra("id",id_location);
                startActivity(in);
            }
        });
    }

    public void disable_trailer_button() {
        Button btn = (Button)findViewById(R.id.btnTrailer);
        btn.setEnabled(false);
    }
    public void change_values(){
        txtTitle.setText(title);
        txtVenue.setText(venue);
        txtDate.setText(date);
        txtDesc.setText(desc);
    }
    public void Trailer(View v) {
        runOnUiThread(new Runnable() {

            @Override
            public void run() {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(trailer)));
            }
        });
    }

    class getEventInfo extends AsyncTask<String, String, String> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(Event.this);
            pDialog.setMessage("Samo malo...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        protected String doInBackground(String... args) {


            txtDesc.setMovementMethod(new ScrollingMovementMethod());

            picture = (ImageView)findViewById(R.id.imgEvent);
            JSONObject json = null;
            // Building Parameters
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("id", eventId));

            json = jParser.makeHttpRequest(link, "GET", params);

            // check log cat for response
            Log.d("JSON", json.toString());

            // check for success tag

            try {
                title = json.getString("ime");
                venue = json.getString("lokacija");
                date = json.getString("datum_izvedbe");
                desc = json.getString("opis");

                image_name = json.getString("image_link");
                trailer = json.getString("trailer");

                if(trailer.equals("")){
                    disable_trailer_button();
                }
                id_location = json.getString("id_lokacija");

            } catch (JSONException e) {
                Log.e("JSON exception", e.toString());
                e.printStackTrace();
            } catch (Exception e) {
                Log.e("Exception", e.toString());
                e.printStackTrace();
            }

            return null;
        }

        protected void onPostExecute(String file_url) {
            change_values();
            imgName = "http://yourdomain.net/img/" + image_name;
            new RetrieveImage(picture).execute(imgName);
        }

    }


    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {

            String urlDisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urlDisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
            pDialog.dismiss();
        }
    }


    class RetrieveImage extends AsyncTask<String, Void, Bitmap> {

        ImageView map;

        public RetrieveImage(ImageView _map) {
            this.map = _map;
        }

        protected Bitmap doInBackground(String... urls) {

            Bitmap bitmap = null;
            InputStream is = null;
            try {
                DefaultHttpClient httpClient = new DefaultHttpClient();
                httpClient.getParams().setParameter("http.protocol.content-charset", "UTF-8");
                HttpGet httpGet = new HttpGet(urls[0].replace(" ", "%20"));
                HttpResponse httpResponse = httpClient.execute(httpGet);
                HttpEntity httpEntity = httpResponse.getEntity();
                is = httpEntity.getContent();
                bitmap = BitmapFactory.decodeStream(is);

            } catch (IOException e) {
                e.printStackTrace();
            } catch (NetworkOnMainThreadException e) {
                Log.e("NETWORK ERROR", "Image" + e.toString());
            } catch (NullPointerException e) {
                Log.e("NETWORK ERROR", "Image" + e.toString());
            } catch (RuntimeException e) {
                Log.e("NETWORK ERROR", "Image" + e.toString());
            }

            return bitmap;
        }

        protected void onPostExecute(Bitmap _bitmap) {
            // TODO: check this.exception
            // TODO: do something with the feed
            map.setImageBitmap(_bitmap);
            pDialog.dismiss();
        }
    }
}
