package org.nodinodi.kartekartice.app;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class EventsList extends ActionBarActivity {


    WindowManager wm;
    ListView lv;
    List<EventItem> eventList;
    EventsArrayAdapter adapter;
    private ProgressDialog pDialog;
    String categoryName = "-1", cityName = "-1";
    Boolean flag;
    JSONParser jParser = new JSONParser();
    final String link = "http://yourdomain.net/getEvents.php";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_events_list);

        setupData();
        eventList = new ArrayList<EventItem>();
        lv = (ListView) findViewById(R.id.EventsList);
    }

    private void setupListView() {
        adapter = new EventsArrayAdapter(this, R.layout.events_list_item, eventList, wm);
        lv.setAdapter(adapter);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(final AdapterView<?> adapterView, View view, int i, long l) {
                Intent in = new Intent();
                in.setClass(getApplicationContext(), Event.class);
                in.putExtra("id",eventList.get(i).getId());
                startActivity(in);
            }
        });
    }

    private void setupData() {
        Intent intent;
        intent = getIntent();
        if (intent.hasExtra("category")){
            categoryName = intent.getStringExtra("category");
            setTitle("Kategorija: " + categoryName);
        }
        if (intent.hasExtra("city")){
            cityName = intent.getStringExtra("city");
            setTitle("Grad: " + cityName);
        }


        new EventInfo().execute();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.events, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    class EventInfo extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Dialog
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(EventsList.this);
            pDialog.setMessage("Samo malo...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        /**
         * Creating http request for login
         */
        protected String doInBackground(String... args) {

            JSONObject json = null;
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("category", categoryName));
            params.add(new BasicNameValuePair("city", cityName));

            json = jParser.makeHttpRequest(link, "GET", params);

            Log.d("JSON", json.toString());

            try {

                Integer status = json.getInt("status");
                if (status == 1) {

                    JSONArray ja = json.getJSONArray("events");

                    for (int i = 0; i < ja.length(); i++) {
                        List<NameValuePair> currentEventInfo = new ArrayList<NameValuePair>();
                        JSONObject jObj = ja.getJSONObject(i);

                        currentEventInfo.add(new BasicNameValuePair("id",jObj.getString("id")));
                        currentEventInfo.add(new BasicNameValuePair("naziv",jObj.getString("naziv")));
                        currentEventInfo.add(new BasicNameValuePair("id_lokacija",jObj.getString("id_lokacija")));
                        currentEventInfo.add(new BasicNameValuePair("datum_izvedbe",jObj.getString("datum_izvedbe")));

                        //kategorije.add(trenutnaKategorija);
                        eventList.add(new EventItem(currentEventInfo));
                    }
                    flag = false;

                } else {
                    Log.d("Dino", "Nema kategorija!");
                    runOnUiThread(new Runnable() {

                        @Override
                        public void run() {

                            Toast.makeText(getApplicationContext(), "Nema kategorija!", Toast.LENGTH_LONG).show();
                            flag = true;
                        }
                    });

                }
            } catch (JSONException e) {
                Log.e("JSON exception", e.toString());
                e.printStackTrace();
            } catch (Exception e) {
                Log.e("Exception", e.toString());
                e.printStackTrace();
            }

            return null;
        }

        protected void onPostExecute(String file_url) {
            // dismiss the dialog once done
            pDialog.dismiss();
            setupListView();

        }

    }

    public void goRegister(View v) {
        Intent i = new Intent(getApplicationContext(), Register.class);
        startActivity(i);
    }


}