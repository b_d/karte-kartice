package org.nodinodi.kartekartice.app;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class Cities extends Activity {

    ArrayList<String> cities = new ArrayList<String>();
    ListView lv;
    private ProgressDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cities);
        setTitle("Gradovi");

        lv = (ListView) findViewById(R.id.CitiesList);
        getIntentData();

        final CitiesArrayAdapter adapter = new CitiesArrayAdapter(this, R.layout.cities_list_item, cities);
        lv.setAdapter(adapter);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(final AdapterView<?> adapterView, View view, int i, long l) {

                Intent in = new Intent();
                in.setClass(getApplicationContext(), EventsList.class);
                in.putExtra("city",cities.get(i));
                startActivity(in);
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.categories, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    private void getIntentData() {
        Intent intent;
        intent = getIntent();
        String jsonString = intent.getStringExtra("cities");

        pDialog = new ProgressDialog(Cities.this);
        pDialog.setMessage("Samo malo...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(true);
        pDialog.show();

        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(jsonString);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
                JSONArray ja = jsonObject.getJSONArray("cities");

                for (int i = 0; i < ja.length(); i++) {
                    JSONObject jObj = ja.getJSONObject(i);
                    cities.add(jObj.getString("name"));
                }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        pDialog.dismiss();
    }

    private class CitiesArrayAdapter extends ArrayAdapter<String> {

        HashMap<String, Integer> mIdMap = new HashMap<String, Integer>();
        private List<String> items;

        public CitiesArrayAdapter(Context context, int textViewResourceId, List<String> objects) {

            super(context, textViewResourceId, objects);
            this.items = objects;
        }
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            View v = convertView;
            if (v == null) {
                LayoutInflater vi;
                vi = LayoutInflater.from(getContext());
                v = vi.inflate(R.layout.cities_list_item, null);
            }
            TextView id = (TextView) v.findViewById(R.id.city);
            id.setText(items.get(position));

           return v;

        }

    }


}
