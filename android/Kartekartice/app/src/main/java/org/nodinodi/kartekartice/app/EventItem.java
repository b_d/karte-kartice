package org.nodinodi.kartekartice.app;

import org.apache.http.NameValuePair;

import java.util.ArrayList;
import java.util.List;

public class EventItem {
    List<NameValuePair> list = new ArrayList<NameValuePair>();

    String ID_TAG = "id";
    String NAME_TAG = "naziv";
    String LOCATION_ID_TAG = "id_lokacija";
    String DATE_PERFORMANCE_TAG = "datum_izvedbe";

    public EventItem(List<NameValuePair> _list) {
        this.list = _list;
    }

    private String getCorrectValue(String tag) {
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getName().equals(tag)) {
                return list.get(i).getValue();
            }
        }
        return "";
    }

    public String getName() {
        return getCorrectValue(NAME_TAG);
    }

    public String getId() {
        return getCorrectValue(ID_TAG);
    }

    public String getLocationId() {
        return getCorrectValue(LOCATION_ID_TAG);
    }

    public String getDatePerformance() {
        return getCorrectValue(DATE_PERFORMANCE_TAG);
    }

}
