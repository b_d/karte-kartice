package org.nodinodi.kartekartice.app;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.telephony.PhoneNumberUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dino Bikić on 29.4.2014..
 */
public class Register extends Activity {

    private EditText etUsername, etEmail, etPass1, etPass2;

    private String username, email, pass1, pass2;
    private Button btnRegister;
    private ProgressDialog pDialog;



    final String register_link = "http://yourdomain.net/register.php";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);



        etUsername = (EditText) findViewById(R.id.etUser);
        etEmail = (EditText) findViewById(R.id.etEmail);
        etPass1 = (EditText) findViewById(R.id.etPass1);
        etPass2 = (EditText) findViewById(R.id.etPass2);

        btnRegister = (Button) findViewById(R.id.btnRegister);
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                username = etUsername.getText().toString();
                email = etEmail.getText().toString();
                pass1 = etPass1.getText().toString();
                pass2 = etPass2.getText().toString();

                if (username.equals("") || email.equals("") || pass1.equals("") || pass2.equals("")) {
                    Toast.makeText(getApplicationContext(), "Nećemo tako, sva polja su obavezna!", Toast.LENGTH_LONG).show();
                    return;
                }else {
                    if (pass1.length() >= 6) {
                        if (!pass1.equals(pass2)) {
                            Toast.makeText(getApplicationContext(), "Nećemo tako, oba passworda moraju biti ista!", Toast.LENGTH_LONG).show();
                            etPass1.setText("");
                            etPass2.setText("");
                        }else{
                            new registerUser().execute();
                        }
                    }else {
                        Toast.makeText(getApplicationContext(), "Nećemo tako, password mora imati barem 6 znakova!", Toast.LENGTH_LONG).show();
                        etPass1.setText("");
                        etPass2.setText("");
                    }
                }
            }
        });
    }

    /**
     * This class extends AsynTask and provide asynchronous http request
     */
    private class registerUser extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(Register.this);
            pDialog.setMessage("Registering user. Please wait...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... args) {

            List<NameValuePair> params = new ArrayList<NameValuePair>();

            params.add(new BasicNameValuePair("username", username));
            params.add(new BasicNameValuePair("password", pass1));
            params.add(new BasicNameValuePair("email", email));

            JSONParser jsonparser = new JSONParser();
            JSONObject jsonObject = jsonparser.makeHttpRequest(register_link, "POST", params);
            try {
                String queryStatus = jsonObject.getString("query");

                if (queryStatus.equals("false")) {
                    runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            // TODO Auto-generated method stub
                            // ako je query false onda, upis nije uspjesan
                            Toast.makeText(getApplicationContext(), "Ma već postoji netko s tim imenom / email-om!!", Toast.LENGTH_LONG).show();
                            etUsername.setText("");
                            etEmail.setText("");
                        }
                    });

                } else {
                        runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                //ako je tocan reg je prosla
                                Toast.makeText(getApplicationContext(), "Uspješno si se registrirao!! Sad se ulogiraj!", Toast.LENGTH_LONG).show();
                                try {
                                    Thread.sleep(200);
                                } catch (InterruptedException e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                }
                                //return to main activity
                                finish();
                            }
                        });

                }
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }

        protected void onPostExecute(String file_url) {
            // dismiss the dialog after getting travel info
            pDialog.dismiss();

            finishActivity(0);
        }

    }

}

