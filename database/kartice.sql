-- phpMyAdmin SQL Dump
-- version 2.11.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 17, 2014 at 06:43 PM
-- Server version: 5.1.57
-- PHP Version: 5.2.17

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Database: `a4773532_kartice`
--

-- --------------------------------------------------------

--
-- Table structure for table `event`
--

CREATE TABLE `event` (
  `id_event` int(11) NOT NULL AUTO_INCREMENT,
  `ime` varchar(200) NOT NULL,
  `opis` varchar(5000) NOT NULL,
  `id_kategorija` int(11) NOT NULL,
  `id_lokacija` int(11) NOT NULL,
  `datum_dodano` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `datum_izvedbe` varchar(30) NOT NULL,
  `image_link` varchar(200) NOT NULL,
  `trailer` varchar(100) NOT NULL,
  PRIMARY KEY (`id_event`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=290 ;

--
-- Dumping data for table `event`
--

INSERT INTO `event` VALUES(263, 'Dropkick Murphys', 'Nakon dvije godine ameri  ka celtic-punk skupina iz Massachusettsa vra  a se pred hrvatsku publiku  ali ovaj put ne u klupski prostor ve   na open-air koncert  br   Rasprodana Tvornica kulture na prvom gostovanju u Hrvatskoj bila je pokazatelj da Dropkick Murphys ovdje imaju jaku bazu fanova  stoga je novi susret zakazan na   alati 16  lipnja  br   Ekskluzivnih 616 ulaznica za fan pit po cijeni od 150 kuna u prodaju kre  e na St  Patrick s Day  17  o  ujka  i to samo u Zig Zag Fan Shopu hokeja  kog kluba Medve    ak  koji su partneri koncerta  br   Nakon   to se rasprodaju ekskluzivne ulaznice kre  e redovna prodaja po cijeni od 170 kuna na uobi  ajnim prodajnim mjestima   div ', 1, 130, '2014-06-16 15:53:09', 'Mon, 16 Jun 2014 20:00:00', 'Dropkick MurphysMon 16 Jun 2014 200000.png', '');
INSERT INTO `event` VALUES(264, 'CROWBAR', 'CROWBAR  SAD  br   Da 13 nije uvijek nesretan broj  sigurno   e se slo  iti mnogobrojni obo  avatelji kultnog benda Crowbar  Naime  upravo je toliko godina pro  lo od njihovog prvog i  za sad  jedinog nastupa u Hrvatskoj  Ali udru  ene snage Metalsound-a i Mo  vare pobrinule su se da vi  egodi  nja nastojanja kona  no urode plodom i da se ovi bogovi te  kih riffova ponovo uka  u pred doma  om publikom  Te  ko je prona  i dovoljno epiteta da se opi  e uloga i status ovog iznimnog benda u povijesti underground glazbe  Njihov spor  dubok  te  ak i turoban glazbeni izri  aj smatra se jednim od vrhunaca iznimno kvalitetne i  za ekstremnu glazbu  bitne scene u New Orleansu te je postao inspiracija za mnogobrojne bendove   irom svijeta zasnovane na njihovom zvuku  br    br   Osniva   i glavna figura benda     Kirk Windstein do nedavno je bio i   lan slavnih Down  ali se odlu  io u potpunosti posvetiti svom mati  nom bendu kako bi se mogao fokusirati na ono   to slijedi  A slijedi im zasigurno sjajna budu  nost jer Crowbar upravo ove godine slave   ak 25 godina postojanja  a tik pred zagreba  ki nastup izlazi im i novi album  quot Symmetry in Black quot   br    br   Nesumnjivo   e ovaj dugoo  ekivani ponovni nastup Crowbar izazvati veliki interes svih ljubitelja sludge   doom   stoner metala i kao takav predstavljati jedan od najzna  ajnijih   estokih koncerata godine   br    br   KATRAN  Zagreb  br   MONOX  Rijeka  br    br   Upad u pretprodaji  100 KN  na dan koncerta na blagajni kluba  120 KN br    br   Ulaznice u pretprodaji potra  ite na sljede  im lokacijama  Klub Mo  vara  Trnjanski nasip bb  Maldoror  Lopa  i  eva 3  Dirty Old Shop  Tratinska 18  Rockmark  Berislavi  eva 13  Dallas Music Shop Zagreb  Tkal  i  eva 7  Dallas Music Shop Rijeka  Splitska 2a  Dallas Music Shop Split  Narodni trg 14  Dallas Music Shop Zadar    iroka ulica 2  br    br   Dodatni upiti   a href  mailto bobo metalsoundmagazine com  bobo metalsoundmagazine com  a   br    br   Links  br    a href  http   www mochvara hr program info mocvara- amp -metalsound-music-predstavljaju-140617  rel  nofollow  http   www mochvara hr program info mocvara- amp -metalsound-music-predstavljaju-140617  a  br    a href  https   www facebook com events 275803222586356  fref ts  rel  nofollow  https   www facebook com events 275803222586356  fref ts  a   div ', 1, 131, '2014-06-16 15:53:09', 'Tue, 17 Jun 2014 20:00:00', 'CROWBARTue 17 Jun 2014 200000.png', 'https://www.facebook.com/events/275803222586356/?ref=22');
INSERT INTO `event` VALUES(265, 'PUN KURAC ČAVLI: FULL OF HELL (SAD), D.O.C. (SAD), DISLIKE (PŽ)', 'Centar za mlade   akovec - Prostor vam s ponosom predstavlja grindcore spektakl direktno iz najjebenije dr  ave Amerike  U sklopu svoje europske turneje dolaze   br    br   FULL OF HELL  grindcore  SAD  br    br    a href  http   www fullofhell bandcamp com  rel  nofollow  www fullofhell bandcamp com  a  br    a href  http   www facebook com fullofhell  rel  nofollow  www facebook com fullofhell  a  br    br    quot Klinci quot  iz Amerike oko kojih se zadnjih godina di  e prili  an hype  a kako i ne bi oko benda koji definira razvoj   anra  U samo par godina sviranja zavr  ili na A389 labelu sa samom kremom nihilisti  kog hardcore punka  te su redoviti gosti A389 ro  endanskih proslava  Vo  eni idejom da bend mora konstantno biti na turneji i   im vi  e svirati Full Of Hell su gotovo 365 dana u godini na putu  Iako ve  inom sviraju po Americi  svake godine poku  avaju odraditi bar jednu Europsku turneju   to im je do sada uspje  no polazilo za rukom   pa se tako u lipnju ove godine spremaju za novi posjet Europi  Njihovi divlji i neobuzdani koncerti  prepuni energije i prisnog odnosa s publikom postali su ne  to   to svaki ljubitelj dobre buke mora iskusiti  Do  ivjeti Full Of Hell danas je kao do  ivjeti Napalm Death 1987-e  br    br    br   D O C  Disciples Of Christ   grindcore  SAD  br    br    a href  http   www disciplesofchrist bandcamp com  rel  nofollow  www disciplesofchrist bandcamp com  a  br    a href  http   www facebook com disciplesofchrist202  rel  nofollow  www facebook com disciplesofchrist202  a  br    br   Na Europskoj grindcore sceni jo   prili  no nepoznato ime  ali nakon ove turneje to   e se zasigurno promijeniti  Mra  ni i masni grindcore s pokojim sludge upadom   na bubnju Chris Moore  Coke Bust  Sick Fix  ex-Magrudergrind    bolja preporuka nije potrebna  br    br    br   DISLIKE  grindcore  P    br    br    a href  http   www dislikecore com  rel  nofollow  www dislikecore com  a  br    a href  http   www facebook com dislikecore  rel  nofollow  www facebook com dislikecore  a  br    br   Nakon 6 godina dislajkara ponovno u   akovcu  Grindcore baje iz Po  ege koji ve   vi  e od 15 godina deru najbolji grind na ovim prostorima  Iz godine u godinu sve bolji  a po  to dosta rijetko sviraju Dislike svirke su ne  to   to se ne propu  ta  br    br     lanska iskaznica   abe  br    br   Start  22 00h br    br   Upad  min  donacija   30kn br    br   IN GRIND WE CRUST  VIDIM OSE   div ', 1, 132, '2014-06-16 15:53:09', 'Tue, 17 Jun 2014 22:00:00', 'PUN KURAC ČAVLI FULL OF HELL SAD DOC SAD DISLIKE PŽTue 17 Jun 2014 220000.png', 'https://www.facebook.com/events/755356557816316/');
INSERT INTO `event` VALUES(266, 'Free Stage ', '', 1, 133, '2014-06-16 15:53:10', 'Thu, 19 Jun 2014 11:31:01', 'Free Stage Thu 19 Jun 2014 113101.png', '');
INSERT INTO `event` VALUES(267, 'trees of maine', '', 1, 134, '2014-06-16 15:53:10', 'Thu, 19 Jun 2014 17:59:01', 'trees of maineThu 19 Jun 2014 175901.png', '');
INSERT INTO `event` VALUES(268, 'Solar Pulse Music: Šumovi Protiv Valova (cro) & Coma Stereo (slo)', '', 1, 135, '2014-06-16 15:53:11', 'Thu, 19 Jun 2014 21:00:00', 'Solar Pulse Music Šumovi Protiv Valova cro  Coma Stereo sloThu 19 Jun 2014 210000.png', 'https://www.facebook.com/events/1440000272919082/');
INSERT INTO `event` VALUES(269, 'Zu', ' a href  http   www facebook com vajrazu  rel  nofollow  www facebook com vajrazu  a  br   twitter com zuism br   youtube com user vajrazu br    a href  http   www ipecac org  rel  nofollow  www ipecac org  a  br    a href  http   www latempesta org  rel  nofollow  www latempesta org  a  br    a href  http   www trost at  rel  nofollow  www trost at  a   div ', 1, 136, '2014-06-16 15:53:11', 'Sat, 21 Jun 2014 12:30:01', 'ZuSat 21 Jun 2014 123001.png', '');
INSERT INTO `event` VALUES(270, 'Huntress', '', 1, 137, '2014-06-16 15:53:12', 'Sat, 21 Jun 2014 15:16:01', 'HuntressSat 21 Jun 2014 151601.png', '');
INSERT INTO `event` VALUES(271, 'SPIRITUAL FRONT @ GK JABUKA ', '', 1, 138, '2014-06-16 15:53:12', 'Sat, 21 Jun 2014 20:00:00', 'SPIRITUAL FRONT  GK JABUKA Sat 21 Jun 2014 200000.png', 'https://www.facebook.com/events/604668486306917/');
INSERT INTO `event` VALUES(272, 'INmusic Festival 2014', 'Croatia a biggest open air festival presents its ninth edition  See for yourselves why we are announced as one of the most interesting festivals in Europe  More info on line up  tickets   everything you want to know find on our official website and our Facebook page  Welcome to INmusic Festival   div ', 1, 139, '2014-06-16 15:53:12', 'Mon, 23 Jun 2014 16:00:01', 'INmusic Festival 2014Mon 23 Jun 2014 160001.png', 'http://www.inmusicfestival.com');
INSERT INTO `event` VALUES(273, 'Zabranjeno Pušenje', '', 1, 140, '2014-06-17 18:36:52', 'Sat, 28 Jun 2014 22:30:00', 'Zabranjeno PušenjeSat 28 Jun 2014 223000.png', '');
INSERT INTO `event` VALUES(274, 'Ultra Europe Festival', 'Following a historic  sold out debut edition in 2013  in which over 103 000 sun seekers flocked to Croatia   s idyllic Dalmatian coast from more than 75 countries worldwide  ULTRA EUROPE now gears up for its second annual installment  as it expands to 3 days  br    br   Now taking place over THREE days in the impressive Poljud Stadium  Split  formerly two days  from July 11   13  2014  Ultra Europe will be welcoming both familiar faces and new in 2014  as it sets to firmly cement itself amongst the heavyweights  as a    must do    festival in the annual calendar   div ', 1, 141, '2014-06-17 18:36:52', 'Fri, 11 Jul 2014 07:59:01', 'Ultra Europe FestivalFri 11 Jul 2014 075901.png', 'http://www.ultraeurope.com/home');
INSERT INTO `event` VALUES(275, 'Nina Romić', '', 1, 142, '2014-06-17 18:36:52', 'Thu, 14 Aug 2014 20:29:01', 'Nina RomićThu 14 Aug 2014 202901.png', '');
INSERT INTO `event` VALUES(276, 'Night Beats', 'Danny Lee  quot Blackwell quot  originally from Dallas  Texas founded Night Beats in 2009 when James Traeger moved from Austin  Texas to Seattle  Washington  Named as an homage to Sam Cooke s Magnum Opus the then 2-piece picked up Tarek Wegner who was living in Seattle  The Night Beats toured extensively early on completing multiple North American tours during 2010 and were signed within weeks of self releasing their debut EP  the H-Bomb EP  Picked up by Chicago s Trouble in Mind Records  Ty Segall  Fresh and Only s  Hex Dispensers  etc    br    They have toured with groups such as The Black Angels  band   in the UK  Europe and U S    Roky Erickson  The Zombies  The Jesus and Mary Chain  The Strange Boys  Black Lips  and the The Growlers  br    Their H-Bomb 7 quot  topped several college radio charts competing with other Artists  full-length efforts   September 6  2010  br    In 2011  they released a split between The UFO Club and Night Beats on Austin s Reverberation Appreciation Society label  In April 2011  the Seattle-based art collective Portable Shrines along with Translinguistic Other released Portable Shrines Magic Sound Theater Vol  1 on the Light In The Attic Records label  a deluxe double LP featuring rare and exclusive tracks from Night Beats  Eternal Tapestry  Prince Rama  AFCGT  and other established and up and coming psychedelic acts curated by Seattle s Portable Shrines Collective  br    On June 28  2011 Trouble In Mind released the Night Beats Self Titled Debut LP  br    In June 2012  They released a single on Volcom Vinyl Club br    In July 2013  The Reverberation Appreciation Society  Austin Psych Fest  announced the release of the full length album  quot Sonic Bloom quot  for September 24th of that year   div ', 1, 143, '2014-06-17 18:38:28', 'Tue, 17 Jun 2014 21:00:00', 'Night BeatsTue 17 Jun 2014 210000.png', 'http://www.facebook.com/events/1423948967858287');
INSERT INTO `event` VALUES(277, 'Irena Tomažin', 'Mlade rime 007  Festival mlade  neuveljavljene poezije  alternativnega zvo  enja  amp  ostalih ustvarjalnosti br    br   Od   upka do zvezd br   16    30  junij 2014  Menza pri koritu  AKC Metelkova mesto  Ljubljana br    br   Berejo  Neja Tom  i    Aja Zamolo  Simona Kopin  ek  br   Gostja ve  era  Kristina Ho  evar  br    br   Glasba  Irena Toma  in  Ne  a Nagli    Miha Ciglar   div ', 1, 144, '2014-06-17 18:38:28', 'Tue, 17 Jun 2014 21:00:00', 'Irena TomažinTue 17 Jun 2014 210000.png', 'http://www.menzaprikoritu.org/?p=27704');
INSERT INTO `event` VALUES(278, 'Huntress', '', 1, 145, '2014-06-17 18:38:28', 'Fri, 20 Jun 2014 15:57:01', 'HuntressFri 20 Jun 2014 155701.png', '');
INSERT INTO `event` VALUES(279, 'Grrizli Madams', 'Mlade rime 007  Festival mlade  neuveljavljene poezije  alternativnega zvo  enja  amp  ostalih ustvarjalnosti br    br   Od   upka do zvezd br   16    30  junij 2014 br   Menza pri koritu  AKC Metelkova mesto  Ljubljana br    br   Berejo  Bla   Ir  i    Katarina Ana Raku    ek  Ana Svetel  Glorjana Veber  Iztok Vren  ur  br   Gost ve  era  Michael Farrell  Avstralija  br    br   Glasba  Grrizli Madams  o O o  It   s Everyone Else   div ', 1, 144, '2014-06-17 18:38:29', 'Fri, 20 Jun 2014 21:00:00', 'Grrizli MadamsFri 20 Jun 2014 210000.png', 'http://www.menzaprikoritu.org/?p=27720');
INSERT INTO `event` VALUES(280, 'Pearl Jam', 'Pearl Jam will embark on an eleven-date European tour in June and July of 2014  The bands summer tour kicks off on June 16th in Amsterdam  NL and wraps up on July 11th in Milton Keynes  UK  br    br   A special ticket pre-sale begins today for current Pearl Jam Ten Club members  as of December 12th  2013   The public on-sale will begin Friday  December 20th  Specific ticket on-sale times and locations will be announced in local markets   div ', 1, 146, '2014-06-17 18:38:29', 'Sun, 22 Jun 2014 20:00:00', 'Pearl JamSun 22 Jun 2014 200000.png', 'http://pearljam.com/tour');
INSERT INTO `event` VALUES(281, 'Steven Seagal Blues Band', '', 1, 147, '2014-06-17 18:38:30', 'Mon, 23 Jun 2014 20:00:00', 'Steven Seagal Blues BandMon 23 Jun 2014 200000.png', '');
INSERT INTO `event` VALUES(282, 'Billy Idol', '', 1, 148, '2014-06-17 18:38:30', 'Tue, 24 Jun 2014 11:00:01', 'Billy IdolTue 24 Jun 2014 110001.png', '');
INSERT INTO `event` VALUES(283, 'Death: DTA', '', 1, 149, '2014-06-17 18:38:30', 'Tue, 24 Jun 2014 20:00:00', 'Death DTATue 24 Jun 2014 200000.png', 'http://www.dirtyskunks.org/en/events.html#death');
INSERT INTO `event` VALUES(284, 'Silverstein', 'Ne bo ravno MOSH BEACH PARTY  bo pa oldschool   pil v Channel Zero  kjer boste lahko stagedivali na potenco   D br   Ve   informacij kmalu    br    br   SILVERSTEIN br    a href  http   www silversteinmusic com   rel  nofollow  http   www silversteinmusic com   a  br    a href  https   www facebook com silversteinmusic  rel  nofollow  https   www facebook com silversteinmusic  a  br    br   BLESSTHEFALL br    a href  http   www blessthefallmusic com   rel  nofollow  http   www blessthefallmusic com   a  br    a href  https   www facebook com blessthefall rf 108332995867805  rel  nofollow  https   www facebook com blessthefall rf 108332995867805  a  br    br   LETLIVE br    a href  http   www thisisletlive com   rel  nofollow  http   www thisisletlive com   a  br    a href  https   www facebook com theletlive  rel  nofollow  https   www facebook com theletlive  a   div ', 1, 143, '2014-06-17 18:38:30', 'Tue, 24 Jun 2014 20:30:00', 'SilversteinTue 24 Jun 2014 203000.png', 'https://www.facebook.com/events/444650402327582/');
INSERT INTO `event` VALUES(285, 'Zu (IT, ZDA) + Y (SLO)', ' a href  http   www facebook com vajrazu  rel  nofollow  www facebook com vajrazu  a  br   twitter com zuism br   youtube com user vajrazu br    a href  http   www ipecac org  rel  nofollow  www ipecac org  a  br    a href  http   www latempesta org  rel  nofollow  www latempesta org  a  br    a href  http   www trost at  rel  nofollow  www trost at  a  br    br     br    br   Y   why  br    br    a href  http   www yband si   rel  nofollow  http   www yband si   a  br    a href  http   yband bandcamp com album blessing-alarm-2011  rel  nofollow  http   yband bandcamp com album blessing-alarm-2011  a  br    a href  http   www facebook com Yband  rel  nofollow  http   www facebook com Yband  a  br    br    a href  http   www youtube com watch v wfxSUdcFtFM  rel  nofollow  http   www youtube com watch v wfxSUdcFtFM  a  br    a href  http   www youtube com watch v zm wpUaK1QA  rel  nofollow  http   www youtube com watch v zm wpUaK1QA  a  br    br   Y je tri  lanska instrumentalna zasedba s severovzhodnega konca Slovenije  ki obstoji od leta 2004  Njihova muzika inkorporira elemente math- in post rocka  psihedelije in ambientalnega pristopa  V desetih letih so imeli   e   tevilne nastope  nekajkrat prekri  arili Slovenijo  gostovali pa so tudi pod dr  avah nekdanje Jugoslavije  v Avstriji  Nem  iji  Slova  ki    e  ki in na Mad  arskem  So ena izmed redkih tukaj  njih zasedb  ki je igrala v slavnem dunajske klubu Porgy  amp  Bess - tam so nastopili na prostem  v okviru dogodka Simulation of the Future Traffic  br    br   Diapazon njihove pojavnosti je med jazzovskim klubom in Triglavskim narodnim parkom  br    br   Y so leta 2006 za prekmursko zalo  bo God Bless This Mess Records izdali EP Germ  a href  http   yband bandcamp com album germ-2006  rel  nofollow  http   yband bandcamp com album germ-2006  a  in eno leto kasneje sodelovali z mladim elektrofonikom  ki sli  i na ime Neuf Meuf  a href  http   yband bandcamp com album y-neuf-meuf-2007  rel  nofollow  http   yband bandcamp com album y-neuf-meuf-2007  a   Tedaj jih je Radio   tudent vklju  il tudi na svoj slavni Klubski maraton   a href  http   www radiostudent si klubskimaraton  rel  nofollow  http   www radiostudent si klubskimaraton  a   Prvi uradni album so Y izdali - prav tako za God Bless This Mess Records - leta 2011  Naslovili so ga z Blessing Alarm   a href  http   yband bandcamp com album blessing-alarm-2011  rel  nofollow  http   yband bandcamp com album blessing-alarm-2011  a    div ', 1, 150, '2014-06-17 18:38:30', 'Tue, 24 Jun 2014 21:00:00', 'Zu IT ZDA  Y SLOTue 24 Jun 2014 210000.png', 'https://www.facebook.com/events/1450264765219980');
INSERT INTO `event` VALUES(286, 'Fishing On Orfű 2014', '', 1, 151, '2014-06-17 18:40:58', 'Wed, 18 Jun 2014 19:38:01', 'Fishing On Orfű 2014Wed 18 Jun 2014 193801.png', '');
INSERT INTO `event` VALUES(287, 'Urban Fest Osijek - UFO 2014.', ' a href  http   www osijek031 com osijek php topic id 49572  rel  nofollow  http   www osijek031 com osijek php topic id 49572  a   div ', 1, 152, '2014-06-17 18:40:58', 'Wed, 18 Jun 2014 21:36:01', 'Urban Fest Osijek  UFO 2014Wed 18 Jun 2014 213601.png', 'http://www.osijek031.com/osijek.php?topic_id=49572');
INSERT INTO `event` VALUES(288, 'Rockmaraton 2014', 'Official Website   a href  http   www rockmaraton hu   rel  nofollow  http   www rockmaraton hu   a  br    br   Hetijegy j  lius 5-ig  11990 Ft br   Hetijegy a helysz  nen  14990 Ft br   Napijegy  b  rmely napra   4990 Ft br   S  torjegy   ra   b  rlettel ingyenes    500 Ft br   VIP PASS  csak hetijeggyel egy  tt   rv  nyes   9990 Ft br   Csomagmeg  rz    telefont  lt  ssel   Ingyenes br                                               br   Price of Weekly Pass  br   Pre-Ordered Before July 5th  11990 HUF br    br   Price of Weekly Pass br   On The Spot  14990 HUF br    br   Day Ticket br   Pre-Ordered Or On The Spot  4990 HUF br    br   Tent Pass  br    Free With a Weekly Pass    500 HUF br    br   VIP Pass br    Only Valid With a Weekly Pass   9990 HUF br    br   Check Room br    With Cell Charching Opportunity   Free br    br    a href  http   www ticketportal hu event aspx ID 43881  rel  nofollow  www ticketportal hu event aspx ID 43881  a   div ', 1, 153, '2014-06-17 18:40:58', 'Mon, 07 Jul 2014 16:04:01', 'Rockmaraton 2014Mon 07 Jul 2014 160401.png', 'https://www.facebook.com/rockmaraton');
INSERT INTO `event` VALUES(289, 'Pannonian Challenge XV', '', 1, 154, '2014-06-17 18:40:58', 'Wed, 13 Aug 2014 00:11:01', 'Pannonian Challenge XVWed 13 Aug 2014 001101.png', 'http://www.facebook.com/events/191541401043810');

-- --------------------------------------------------------

--
-- Table structure for table `kategorije`
--

CREATE TABLE `kategorije` (
  `id_kategorija` int(11) NOT NULL AUTO_INCREMENT,
  `naziv` varchar(20) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id_kategorija`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `kategorije`
--

INSERT INTO `kategorije` VALUES(1, 'Koncert');
INSERT INTO `kategorije` VALUES(2, 'Predstava');
INSERT INTO `kategorije` VALUES(3, 'Sport');

-- --------------------------------------------------------

--
-- Table structure for table `kupljene_karte`
--

CREATE TABLE `kupljene_karte` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_event` int(11) NOT NULL,
  `id_user` varchar(20) CHARACTER SET utf8 NOT NULL,
  `datum` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=23 ;

--
-- Dumping data for table `kupljene_karte`
--

INSERT INTO `kupljene_karte` VALUES(20, 268, 'a', '2014-06-16 16:54:34');

-- --------------------------------------------------------

--
-- Table structure for table `lokacija`
--

CREATE TABLE `lokacija` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `naziv` varchar(30) CHARACTER SET utf8 NOT NULL,
  `ulica` varchar(40) CHARACTER SET utf8 NOT NULL,
  `naselje` varchar(30) CHARACTER SET utf8 NOT NULL,
  `postanski_broj` varchar(10) COLLATE latin1_general_ci NOT NULL,
  `drzava` varchar(20) CHARACTER SET utf8 NOT NULL,
  `geo_sirina` varchar(15) COLLATE latin1_general_ci NOT NULL,
  `geo_duzina` varchar(15) COLLATE latin1_general_ci NOT NULL,
  `website` varchar(30) COLLATE latin1_general_ci NOT NULL,
  `opis_lokacije` varchar(500) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=155 ;

--
-- Dumping data for table `lokacija`
--

INSERT INTO `lokacija` VALUES(130, 'Šalata', 'Schlosserove stube 2', 'Zagreb', '', 'Croatia', '45.818165', '15.984927', 'http://www.last.fm/venue/87913', '');
INSERT INTO `lokacija` VALUES(131, 'Močvara', 'Trnjanski Nasip bb', 'Zagreb', '', 'Croatia', '45.8', '16', 'http://www.last.fm/venue/87830', '');
INSERT INTO `lokacija` VALUES(132, 'Prostor', 'Ulica dr. Ivana Novaka 38', 'Čakovec', '', 'Croatia', '46.386668', '16.422585', 'http://www.last.fm/venue/10297', '');
INSERT INTO `lokacija` VALUES(133, 'Retro caffe & night bar', 'Savska cesta 180', 'Zagreb', '', 'Croatia', '45.787608', '15.954203', 'http://www.last.fm/venue/92187', '');
INSERT INTO `lokacija` VALUES(134, 'booksa', '', 'Zagreb', '', 'Croatia', '45.816301', '15.978327', 'http://www.last.fm/venue/87904', '');
INSERT INTO `lokacija` VALUES(135, 'Pekarna - Gustaf', 'Ob Železnici 8', 'Maribor', '', 'Slovenia', '46.552093', '15.642963', 'http://www.last.fm/venue/87954', '');
INSERT INTO `lokacija` VALUES(136, 'KSET', 'Unska 3', 'Zagreb', '', 'Croatia', '45.80216', '15.970159', 'http://www.last.fm/venue/87798', '');
INSERT INTO `lokacija` VALUES(137, 'Attack!, AKC Medika', 'Pierottijeva 11', 'Zagreb', '', 'Croatia', '45.8062', '15.964311', 'http://www.last.fm/venue/91992', '');
INSERT INTO `lokacija` VALUES(138, 'GK Jabuka', 'Jabukovac 28', 'Zagreb', '', 'Croatia', '45.823584', '15.972078', 'http://www.last.fm/venue/10256', '');
INSERT INTO `lokacija` VALUES(139, 'Jarun', '', 'Zagreb', '', 'Croatia', '45.785979', '15.924366', 'http://www.last.fm/venue/89174', '');
INSERT INTO `lokacija` VALUES(140, 'OHara', 'Uvala Zenta 3', 'Split', '', 'Croatia', '43.5175', '16.4272', 'http://www.last.fm/venue/88301', '');
INSERT INTO `lokacija` VALUES(141, 'Stadion Poljud', '', 'Split', '', 'Croatia', '43.5206', '16.4315', 'http://www.last.fm/venue/88877', '');
INSERT INTO `lokacija` VALUES(142, 'Azimut', 'Obala palih omladinaca 2', 'Šibenik', '', 'Croatia', '43.736031', '15.888679', 'http://www.last.fm/venue/10327', '');
INSERT INTO `lokacija` VALUES(143, 'Channel Zero', 'Metelkova 4 (AKC Metelkova)', 'Ljubljana', '', 'Slovenia', '46.051356', '14.501025', 'http://www.last.fm/venue/87807', '');
INSERT INTO `lokacija` VALUES(144, 'Menza Pri Koritu', 'Metelkova ulica 4', 'Ljubljana', '', 'Slovenia', '46.056215', '14.515725', 'http://www.last.fm/venue/92193', '');
INSERT INTO `lokacija` VALUES(145, 'Orto bar', 'Grablovičeva 1', 'Ljubljana', '', 'Slovenia', '46.058231', '14.522518', 'http://www.last.fm/venue/87808', '');
INSERT INTO `lokacija` VALUES(146, 'Stadio Nereo Rocco', 'Piazzale Atleti Azzurri, 5', 'Trieste', '', 'Italy', '45.623417', '13.792964', 'http://www.last.fm/venue/10230', '');
INSERT INTO `lokacija` VALUES(147, 'Cvetličarna Mediapark', 'Kranjčeva 20', 'Ljubljana', '', 'Slovenia', '46.070541', '14.521129', 'http://www.last.fm/venue/87840', '');
INSERT INTO `lokacija` VALUES(148, 'Hala Tivoli', 'Celovška cesta 25', 'Ljubljana', '', 'Slovenia', '46.059833', '14.498146', 'http://www.last.fm/venue/87829', '');
INSERT INTO `lokacija` VALUES(149, 'Gala Hala', 'Metelkova 4 (AKC Metelkova)', 'Ljubljana', '', 'Slovenia', '46.051356', '14.501025', 'http://www.last.fm/venue/87811', '');
INSERT INTO `lokacija` VALUES(150, 'Klub Gromka', 'Metelkova 4 (AKC Metelkova)', 'Ljubljana', '', 'Slovenia', '46.0552778', '14.5144444', 'http://www.last.fm/venue/87899', '');
INSERT INTO `lokacija` VALUES(151, 'Panoráma Camping', 'Dollár u. 1.', 'Orfű', '', 'Hungary', '46.144932', '18.140146', 'http://www.last.fm/venue/89670', '');
INSERT INTO `lokacija` VALUES(152, 'Dvorište Vega', '', 'Osijek', '', 'Croatia', '45.5511111', '18.6938889', 'http://www.last.fm/venue/89367', '');
INSERT INTO `lokacija` VALUES(153, 'Malomvölgyi Arborétum', '', 'Pécs', '', 'Hungary', '46.0833333', '18.2333333', 'http://www.last.fm/venue/89181', '');
INSERT INTO `lokacija` VALUES(154, 'Copacabana', '', 'osijek', '', 'Croatia', '45.5511111', '18.6938889', 'http://www.last.fm/venue/90495', '');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `username` varchar(20) NOT NULL,
  `password` varchar(32) NOT NULL,
  `email` varchar(40) NOT NULL,
  `date_added` varchar(20) NOT NULL,
  `verify` tinyint(4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` VALUES('dino', '0cc175b9c0f1b6a831c399e269772661', 'nodiizlagune@gmail.com', '', 1);
INSERT INTO `users` VALUES('ad', '76d80224611fc919a5d54f0ff9fba446', 'qwe@qwe.qwe', '', 1);
INSERT INTO `users` VALUES('e', '08a4415e9d594ff960030b921d42b91e', '4r', '2014-04-29 17:40:03', 1);
INSERT INTO `users` VALUES('ee', '80007f222f88c4460eff8f919afa56d1', '4er', '2014-04-29 17:40:56', 1);
INSERT INTO `users` VALUES('ii', '343b1c4a3ea721b2d640fc8700db0f36', 'ii', '2014-04-29 17:58:41', 1);
INSERT INTO `users` VALUES('a', '0cc175b9c0f1b6a831c399e269772661', 'a@a.com', 'aaa', 1);
